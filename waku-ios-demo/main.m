//
//  main.m
//  waku-ios-demo
//
//  Created by Éric Platon on 2014/05/14.
//  Copyright (c) 2014年 WAKU WAKU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WakuDemoAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([WakuDemoAppDelegate class]));
    }
}
