//
//  WakuCouponViewController.m
//  waku-ios-demo
//
//  Created by Éric Platon on 2014/05/28.
//  Copyright (c) 2014年 WAKU WAKU. All rights reserved.
//

#import "WakuCouponViewController.h"

@interface WakuCouponViewController ()

@end

@implementation WakuCouponViewController {
    UIImage * couponImage;
    WakuSDK * wakuSDK;
    WakuCoupon * currentCoupon;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.couponImageView setBackgroundColor:[UIColor redColor]];
    if (couponImage) {
        [self showCouponImage];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) couponReady:(WakuCoupon *)coupon {
    NSLog(@"Got coupon: %@", coupon);
    NSLog(@"Coupon image size: (%d, %d)", (int)[coupon.image size].width, (int)[coupon.image size].height);
    NSLog(@"Image file size: %lu", (unsigned long)[UIImagePNGRepresentation(coupon.image) length]);
    couponImage = coupon.image;
    if (self.couponImageView != nil) {
        [self showCouponImage];
    }
    currentCoupon = coupon;
}

- (void) setWakuSDK:(WakuSDK *)sdk {
    wakuSDK = sdk;
}

- (void) noCouponThisTime:(NSString *)information {
    NSLog(@"No coupon this time: %@", information);
}

- (void) emailSent:(NSString *)email {
    NSLog(@"Email sent: %@", email);
}

- (void) emailSendingFailure:(NSString *)errorMessage {
    NSLog(@"Email failed: %@", errorMessage);
}

- (void) showCouponImage {
    if (self.couponImageView && couponImage) {
        [self.couponImageView setImage:couponImage];
        [self.couponImageView setNeedsDisplay];
    }
}

- (IBAction) dismissView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction) sendEmail:(id)sender {
    if (wakuSDK && currentCoupon) {
        [wakuSDK postCouponByEmail:currentCoupon email:self.emailAddress.text delegate:self];
        [self dismissView:self];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [self sendEmail:textField.text];
    return NO;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField {
    [self animateTextField:textField];
}

- (void) animateTextField:(UITextField*)textField {
    UIScreen * screen = [UIScreen mainScreen];
    
    UIInterfaceOrientation orient = [[UIApplication sharedApplication] statusBarOrientation];
    int movement = [screen bounds].size.height / 2 ;
    if (orient == UIInterfaceOrientationPortrait || orient == UIInterfaceOrientationPortraitUpsideDown) {
        self.view.frame = CGRectOffset(self.view.frame, 0, -movement);
    } else if (orient == UIInterfaceOrientationLandscapeRight) {
        movement = [screen bounds].size.width / 2;
        self.view.frame = CGRectOffset(self.view.frame, movement, 0);
    } else if (orient == UIInterfaceOrientationLandscapeLeft) {
        movement = [screen bounds].size.width / 2;
        self.view.frame = CGRectOffset(self.view.frame, -movement, 0);
    }

    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: 0.3f];
    [UIView commitAnimations];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
