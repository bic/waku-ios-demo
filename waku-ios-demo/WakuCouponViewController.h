//
//  WakuCouponViewController.h
//  waku-ios-demo
//
//  Created by Éric Platon on 2014/05/28.
//  Copyright (c) 2014年 WAKU WAKU. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WakuSDK/WakuSDK.h"

@interface WakuCouponViewController : UIViewController<WakuCouponStateProtocol>

@property IBOutlet UIImageView * couponImageView;
@property IBOutlet UITextField * emailAddress;

- (void) setWakuSDK:(WakuSDK *)sdk;

- (IBAction) dismissView:(id)sender;
- (IBAction) sendEmail:(id)sender;

@end
