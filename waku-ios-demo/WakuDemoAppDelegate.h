//
//  WakuDemoAppDelegate.h
//  waku-ios-demo
//
//  Created by Éric Platon on 2014/05/14.
//  Copyright (c) 2014年 WAKU WAKU. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WakuDemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
