//
//  WakuDemoViewController.m
//  waku-ios-demo
//
//  Created by Éric Platon on 2014/05/14.
//  Copyright (c) 2014年 WAKU WAKU. All rights reserved.
//

#import "WakuDemoViewController.h"

@interface WakuDemoViewController ()

#define COMPANY_ID @"9b20d219e157c51f13cdba2ca5b158dda4f1702c0ee34fe0059314fe65bd0b647b0f22b2ff7704d0064c48c120b7fa4940af19f8b2d62479944cb0785e7aa1d8"
#define APP_ID @"c0d70ff5-2aca-4116-afa7-e6173cdc7584"

@property (weak, nonatomic) IBOutlet UIButton *getCouponButton;

@end

@implementation WakuDemoViewController {
    WakuSDK * wakuSDK;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	self.sdkStateIndicator.text = @"The SDK is getting ready";
    [self.sdkStateIndicator sizeToFit];
    wakuSDK = [WakuSDK
               activate:COMPANY_ID
               appId:APP_ID
               orientation:@"portrait"
               productionMode:NO];
    self.sdkStateIndicator.text = @"The SDK is ready";
    [self.sdkStateIndicator sizeToFit];
}

/* Future async init
- (void) activated:(id)sdk {
    wakuSDK = sdk;
    self.sdkStateIndicator.text = @"The SDK is ready";
    [self.sdkStateIndicator sizeToFit];
}

- (void) failedActivation:(NSError *)error {
    wakuSDK = nil;
    self.sdkStateIndicator.text = @"The SDK failed to initialize. No coupon can be served.";
    [self.sdkStateIndicator sizeToFit];
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (wakuSDK) {
        WakuCouponViewController * destination = [segue destinationViewController];
        [wakuSDK requestCoupon:destination];
        [destination setWakuSDK:wakuSDK];
    }
}

@end
